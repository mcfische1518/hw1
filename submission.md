####  My name is Michael Fischer

This is my submission for homework1. 
I would like to include one link: [homework 1](git@gitlab.com:mcfische1518/hw1.git)

I would also like to include some facts in a table:

|   Item        |          Info            |
|---------------|--------------------------|
| home town     | <Keller Texas> 		 |
| favorite song | <Atlantic City by The Band> 	 |
| <anything else> | <Looking forward to learning the 2021 and a my final semester at Ole Miss> |
